# ratemon-api

Prepare the virtual env and install dependencies:

```bash
pipenv shell
pipenv install
```

Repository deprecated. Code was merged into upstream [CMS RateMon software](https://gitlab.cern.ch/cms-tsg-fog/ratemon).
