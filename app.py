import json

def rawRates(runNumber: int, triggerKey: str):
	# TODO: sandbox this
	with open('run'+str(runNumber)+"_collisions.json") as jsonfile:
		data = json.load(jsonfile)
		return data[triggerKey]